from launch import LaunchDescription
from launch_ros.actions import Node, ComposableNodeContainer
from launch_ros.descriptions import ComposableNode
from ament_index_python.packages import get_package_share_path
import xacro


def generate_mops_urdf():
  xacro_path = get_package_share_path('mops_description') / 'urdf' / 'mops_sdu.urdf.xacro'
  doc = xacro.process_file(xacro_path)
  robot_description = doc.toxml()
  return robot_description


def generate_launch_description():
  robot_description = generate_mops_urdf()
  rviz_config_path = get_package_share_path('mops_rviz') / 'rviz' / 'mops_jog.rviz'
  
  robot_state_publisher = Node(
    package='robot_state_publisher',
    executable='robot_state_publisher',
    parameters=[{'robot_description': robot_description}]
  )
  
  joint_state_publisher_gui = Node(
    package='joint_state_publisher_gui',
    executable='joint_state_publisher_gui'
  )
  
  rviz = Node(
    package='rviz2',
    executable='rviz2',
    arguments=['-d', f'{rviz_config_path}']
  )

  return LaunchDescription([
    robot_state_publisher,
    joint_state_publisher_gui,
    rviz,
  ])
