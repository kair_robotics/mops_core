from launch import LaunchDescription
from launch_ros.actions import Node, ComposableNodeContainer
from launch_ros.descriptions import ComposableNode
from launch.actions import DeclareLaunchArgument
#from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition, UnlessCondition
from launch.substitutions import LaunchConfiguration
from launch.substitutions import PythonExpression
from launch.substitutions import PathJoinSubstitution
from launch_ros.substitutions import FindPackageShare
from launch.launch_description_sources import PythonLaunchDescriptionSource
from ament_index_python.packages import get_package_share_path
from ament_index_python.packages import get_package_share_directory
import xacro


def generate_urdf(xacro_path):
  
  doc = xacro.process_file(xacro_path)
  robot_description = doc.toxml()
  return robot_description


def generate_launch_description():
  robot_description = generate_urdf(get_package_share_path('mops_description') / 'urdf' / 'mops_but_ur5e_si.urdf.xacro')
  gello_description = generate_urdf(get_package_share_path('mops_gello_description') / 'urdf' / 'gello_macro.urdf.xacro')
  rviz_config_path = get_package_share_path('mops_rviz') / 'rviz' / 'mops_but_teleop.rviz'
  
  rcm_pose_publisher_node = Node(
    package='tf2_ros',
    executable='static_transform_publisher',
    arguments=['0.0', '0.5', '0.184', '0', '0', '0', 'world', 'rcm']
  )
  
  ur_control_node = Node(
    package='ur_rtde_ros',
    namespace='ur',
    executable='ur_control',
    parameters=[{
      'hostname': '192.168.56.101',
      'prefix': 'ur_',
      'servo_rate_hz': 125.0,
      'servo_j_lookahead_time': 0.2,
    }]
  )
  
  eua_control_node = Node(
    package='eua_control',
    namespace='/tool',
    executable='fake_eua',
    output='screen',
  )
  
  gello_control_node = Node(
    package='mops_gello_control',
    executable='mops_gello_control',
    namespace='gello',
    parameters=[{
      'dev123_name': '/dev/serial/by-id/usb-ROBOTIS_OpenRB-150_6720061450304A46462E3120FF0B2812-if00',
      'dev456_name': '/dev/serial/by-id/usb-ROBOTIS_OpenRB-150_699EEC0750555347372E3120FF093319-if00',
      'motor_offsets': [3066, 3064, 2563, 3560, 3580, 2500, 170],
      'motor_coeffs': [0.0015344483658124902, 0.0015344483658124902, 0.0015344483658124902, 0.0015344483658124902, 0.0015344483658124902, 0.0015344483658124902, 0.005814814814814815],
      'load_limits': [2.4, 2.4, 2.4, 0.96, 0.96, 0.96, 1.0],
      'robot_description': gello_description,
      'start_enabled': True,
      'start_in_initial_configuration': True,
      'initial_configuration': [0.0, 0.0, -1.57, -4.71, -1.57, 0.0, 0.8],
      'return_to_initial_position_on_enable': True,
      'default_velocity': 0.5,
    }],
    remappings=[
      ('enable', 'not_clutch'),
    ],
    output='screen',
  )
  
  mops_footpedal_node = Node(
    package='mops_footpedal',
    executable='mops_footpedal',
    namespace='gello',
    parameters=[{
      'device': '/dev/input/by-id/usb-PCsensor_FootSwitch-event-kbd',
    }],
    remappings=[
      ('clutch', '/clutch_engaged'),
    ],
    output='log',
  )
  
  mops_teleop_node = Node(
    package='mops_teleop',
    executable='mops_teleop',
    parameters=[{
      'ur_prefix': 'ur_',
      'haptic_prefix': 'gello_',
      'orientation_mode': 1,
    }],
    remappings=[
      #('haptic_pose', 'pose_received'),
      #('haptic_buttons', 'buttons_received'),
      ('ee_state_desired', 'servo_joint_ik'),
      ('ee_state_haptic', '/gello/ee_state'),
    ],
  )  

  mops_control_node = Node(
    package='mops_control',
    executable='mops_control',
    parameters=[{
      'robot_description': robot_description,
      'ur_prefix': 'ur_',
      'tool_prefix': 'tool_',
      'alpha': 1.0,
      'max_iterations': 1,
      'control_mode': 0, # START IN FREE MODE
      'tcp_trans_weights': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.1, 0.1, 0.1],
      'tcp_rot_weights': [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 1.0, 1.0, 1.0],
      'rcm_trans_weights': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 100.0],
    }],
    remappings=[
      ('/ur/servo_joint_default', '/ur/servo_joint'),
    ],
    output='screen'
  )
  
  mops_state_publisher_node = Node(
    name='mops_state_publisher',
    package='mops_state_publisher',
    executable='mops_state_publisher',
    parameters=[{
      'robot_description': robot_description
    }],
    output='screen',
  )
  
  mops_gui_node = Node(
    package='mops_teleop_tools',
    executable='mops_gui',
  )
  
  rviz_node = Node(
    package='rviz2',
    executable='rviz2',
    arguments=['-d', f'{rviz_config_path}'],
  )
  
  mops_teleop_gui_node = Node(
    package='mops_teleop_tools',
    executable='mops_teleop_gui',
    parameters=[{
      'rate': 0.008,
      'base_frame': 'world',
      'tcp_frame': 'tool_ee_tcp_desired',
      'pos_offset': [0.1, 0.40, 0.1],
      'rpy_offset': [0.0, 1.57, 0.0],
    }],
    remappings=[
      ('tool_ee_state_desired', 'servo_joint_ik'),
    ],
    output='screen',
  )

  return LaunchDescription([
    rcm_pose_publisher_node,
    ur_control_node,
    eua_control_node,
    gello_control_node,
    mops_footpedal_node,
    mops_teleop_node,
    mops_control_node,
    mops_state_publisher_node,
    rviz_node,
    mops_gui_node,
    mops_teleop_gui_node,
  ])
