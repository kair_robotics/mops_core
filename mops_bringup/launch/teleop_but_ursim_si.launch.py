from launch import LaunchDescription
from launch_ros.actions import Node, ComposableNodeContainer
from launch_ros.descriptions import ComposableNode
from launch.actions import DeclareLaunchArgument
#from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition, UnlessCondition
from launch.substitutions import LaunchConfiguration
from launch.substitutions import PythonExpression
from launch.substitutions import PathJoinSubstitution
from launch_ros.substitutions import FindPackageShare
from launch.launch_description_sources import PythonLaunchDescriptionSource
from ament_index_python.packages import get_package_share_path
from ament_index_python.packages import get_package_share_directory
import xacro


def generate_urdf(xacro_path):
  
  doc = xacro.process_file(xacro_path)
  robot_description = doc.toxml()
  return robot_description


def generate_launch_description():
  robot_description = generate_urdf(get_package_share_path('mops_description') / 'urdf' / 'mops_but_ur5e_si.urdf.xacro')
  rviz_config_path = get_package_share_path('mops_rviz') / 'rviz' / 'mops_but_teleop.rviz'
  
  rcm_pose_publisher_node = Node(
    package='tf2_ros',
    executable='static_transform_publisher',
    arguments=['0.0', '0.5', '0.184', '0', '0', '0', 'world', 'rcm']
  )
  
  ur_control_node = Node(
    package='ur_rtde_ros',
    namespace='ur',
    executable='ur_control',
    parameters=[{
      'hostname': '192.168.56.101',
      'prefix': 'ur_',
      'servo_rate_hz': 125.0,
      'servo_j_lookahead_time': 0.2,
    }]
  )
  
  eua_control_node = Node(
    package='eua_control',
    namespace='/tool',
    executable='fake_eua',
    output='screen',
  )
  
  hmi_client_node = Node(
    package='mops_touch_server',
    executable='mops_touch_client',
    parameters=[{
      'host': '172.17.0.1', # IP from docker network configuration
      'do_publish_buttons': 1,
    }],
  )
  
  mimic_clutch_node = Node( # only relevant when using Mimic
    package='mops_touch_server',
    executable='mimic_clutch',
  )
  
  mops_teleop_node = Node(
    package='mops_teleop',
    executable='mops_teleop',
    parameters=[{
      'ur_prefix': 'ur_',
      'haptic_prefix': 'touch_',
      'orientation_mode': 1,
    }],
    remappings=[
      ('haptic_pose', 'pose_received'),
      ('haptic_buttons', 'buttons_received'),
      ('ee_state_desired', 'servo_joint_ik'),
    ],
  )  

  mops_control_node = Node(
    package='mops_control',
    executable='mops_control',
    parameters=[{
      'robot_description': robot_description,
      'ur_prefix': 'ur_',
      'tool_prefix': 'tool_',
      'alpha': 1.0,
      'max_iterations': 1,
      'control_mode': 0, # START IN FREE MODE
      'tcp_trans_weights': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.1, 0.1, 0.1],
      'tcp_rot_weights': [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 1.0, 1.0, 1.0],
      'rcm_trans_weights': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 100.0],
    }],
    remappings=[
      ('/ur/servo_joint_default', '/ur/servo_joint'),
    ],
    output='screen'
  )
  
  mops_state_publisher_node = Node(
    name='mops_state_publisher',
    package='mops_state_publisher',
    executable='mops_state_publisher',
    parameters=[{
      'robot_description': robot_description
    }],
    output='screen',
  )
  
  mops_gui_node = Node(
    package='mops_teleop_tools',
    executable='mops_gui',
  )
  
  rviz_node = Node(
    package='rviz2',
    executable='rviz2',
    arguments=['-d', f'{rviz_config_path}'],
  )
  
  mops_teleop_gui_node = Node(
    package='mops_teleop_tools',
    executable='mops_teleop_gui',
    parameters=[{
      'rate': 0.008,
      'base_frame': 'world',
      'tcp_frame': 'tool_ee_tcp_desired',
      'pos_offset': [0.1, 0.40, 0.1],
      'rpy_offset': [0.0, 1.57, 0.0],
    }],
    remappings=[
      ('tool_ee_state_desired', 'servo_joint_ik'),
    ],
    output='screen',
  )

  return LaunchDescription([
    rcm_pose_publisher_node,
    ur_control_node,
    eua_control_node,
    hmi_client_node,
    mimic_clutch_node,
    mops_teleop_node,
    mops_control_node,
    mops_state_publisher_node,
    rviz_node,
    mops_gui_node,
    mops_teleop_gui_node,
  ])
