#!/bin/bash
ros2 topic pub /tool/servo_joint sensor_msgs/msg/JointState "header:
  stamp:
    sec: 0
    nanosec: 0
  frame_id: ''
name: ['tool_roll', 'tool_pitch', 'tool_yaw1', 'tool_yaw2']
position: [1.0, -1.0, -0.5, 0.5]" 
