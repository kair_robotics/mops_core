#!/usr/bin/python3

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState


class FakeEUANode(Node):
  def __init__(self):
    super().__init__("fake_eua")
    self.cmd_sub = self.create_subscription(JointState, 'servo_joint', self.cmd_cb, 10)
    self.state_pub = self.create_publisher(JointState, 'joint_states', 10)

  def cmd_cb(self, msg):
    self.state_pub.publish(msg)

def main(args=None):
  rclpy.init(args=args)
  node = FakeEUANode()
  rclpy.spin(node)
  rclpy.shutdown()


if __name__ == "__main__":
  main()

