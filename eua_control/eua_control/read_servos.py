import argparse
import dynamixel
import numpy as np

def main():
	chain = dynamixel.Chain('/dev/serial/by-id/usb-ROBOTIS_OpenRB-150_8D8C860F50304A46462E3120FF0B250C-if00', 1000000, [1, 2, 3, 4])
	i = 3
	list=[]

	for dev in chain.devices:
	  d = dev.read_params(['present_position', 'present_speed'])
	  print(d)
	  list.append(d['present_position'])
	
	print("{'servo_calibration_offset': [", end="")
	print(", ".join(map(str, list[::-1])), end="]},\n")
if __name__ == "__main__":
	main()
