import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
import tkinter as tk
import math 

class JointGoalPublisher(Node):
    def __init__(self):
        super().__init__('joint_goal_publisher')
        self.publisher = self.create_publisher(JointState, '/tool/servo_joint', 10)
        self.init_gui()

    def init_gui(self):
        self.root = tk.Tk()
        self.root.title('Set Joint Goals')
        self.sliders = []
        param = self.declare_parameter("prefix", "")
        if not param:
            raise RuntimeError("The 'prefix' parameter is empty")
        prefix = self.get_parameter("prefix").get_parameter_value().string_value
        self.joint_names = [prefix + n for n in ('roll', 'pitch', 'yaw1', 'yaw2')]
        self.joint_positions = [0.0] * len(self.joint_names) 

        for i, joint_name in enumerate(self.joint_names):
            label = tk.Label(self.root, text=joint_name)
            label.grid(row=i, column=0, padx=5, pady=5)
            slider = tk.Scale(self.root, from_=-0.75, to=0.75, resolution=0.01, orient=tk.HORIZONTAL,
                              command=lambda value, index=i: self.update_joint_position(index, value))
            slider.grid(row=i, column=1, padx=5, pady=5)
            self.sliders.append(slider)

    def update_joint_position(self, index, value):
        self.joint_positions[index] = float(value)
        self.publish_joint_states()

    def publish_joint_states(self):
        msg = JointState()
        msg.name = ['tool_roll', 'tool_pitch', 'tool_yaw0', 'tool_yaw1','tool_yaw2',]
        msg.position = [self.joint_positions[0], self.joint_positions[1], 0.0, self.joint_positions[2], self.joint_positions[3]]
        #msg.velocity = [0.05] * len(self.joint_names) 
        #msg.effort = [0.1] * len(self.joint_names)  
        self.publisher.publish(msg)

    def run(self):
        self.root.mainloop()

def main(args=None):
    rclpy.init(args=args)
    node = JointGoalPublisher()
    rclpy.spin_once(node, timeout_sec=0)
    node.run()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
