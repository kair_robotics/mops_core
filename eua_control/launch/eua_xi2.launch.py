from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='eua_control',
            namespace='/tool',
            executable='controller',
            name='eua_control_b',
            parameters=[
                {'prefix': 'tool_'},
                {'simulated': False},
                {'type': 2},
                #{'port': '/dev/ttyACM1'},
                {'port': '/dev/serial/by-id/usb-ROBOTIS_OpenRB-150_8D8C860F50304A46462E3120FF0B250C-if00'},
                {'baud_rate': 1000000},
                {'joint_device_mapping': {'roll': 4, 'pitch': 3, 'yaw1': 2, 'yaw2': 1}},
                {'homing_load_thresholds': [0.16, 0.16, 0.13, 0.13]},
                {'publish_servo_states': True},
                #{'servo_calibration_offset': [0.74, 3.79, 2.04, 4.45]},
                {'servo_calibration_offset': [3.1224132112601852,1.33488918614071,2.0192116884611322,1.112407655117265]},
            ],
            output='screen',
        ),
    ])
