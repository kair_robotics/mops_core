from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='eua_control',
            namespace='/tool',
            executable='controller',
            name='eua_control_b',
            parameters=[
                {'prefix': 'tool_'},
                {'simulated': False},
                {'type': 1},
                #{'port': '/dev/ttyACM1'},
                {'port': '/dev/serial/by-id/usb-Xevelabs_USB2AX_55533343537351718121-if00'},
                {'baud_rate': 1000000},
                {'joint_device_mapping': {'roll': 1, 'pitch': 2, 'yaw1': 4, 'yaw2': 3}},
                {'homing_load_thresholds': [0.16, 0.16, 0.13, 0.13]},
                {'publish_servo_states': True},
                #{'servo_calibration_offset': [0.74, 3.79, 2.04, 4.45]},
                {'servo_calibration_offset': [2.6179938779914944, 2.6179938779914944, 2.6179938779914944, 2.6179938779914944]},
            ],
            output='screen',
        ),
    ])
