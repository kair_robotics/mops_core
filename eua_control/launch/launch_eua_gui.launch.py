from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='eua_control',
            namespace='/a/tool',
            executable='controller',
            name='eua_control_a',
            parameters=[
                {'prefix': 'a_tool_'},
                {'simulated': False},
                {'type': 2},
                {'port': '/dev/serial/by-id/usb-Xevelabs_USB2AX_55533343537351718121-if00'},
                {'baud_rate': 1000000},
                {'joint_device_mapping': {'roll': 1, 'pitch': 2, 'yaw1': 3, 'yaw2': 4}},
                {'homing_load_thresholds': [0.13, 0.18, 0.13, 0.13]},
            ],
            output='screen',
        ),
    	Node(
	    package='eua_control',
	    namespace='/a/tool',
	    executable='servo_gui',
	    parameters=[
                {'prefix': 'a_tool_'}]
	  )
     
    ])
