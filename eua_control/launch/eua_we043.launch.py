from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='eua_control',
            namespace='/tool',
            executable='controller',
            name='eua_control',
            parameters=[
                {'prefix': 'tool_'},
                {'simulated': False},
                {'type': 1},
                {'port': '/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A9007E0p-if00-port0'},
                {'baud_rate': 1000000},
                {'joint_device_mapping': {'roll': 5, 'pitch': 2, 'yaw1': 6, 'yaw2': 1}},
                {'homing_load_thresholds': [0.16, 0.16, 0.13, 0.13]},
                {'publish_servo_states': True},
                {'servo_calibration_offset': [2.6179938779914944, 2.6179938779914944, 2.6179938779914944, 2.6179938779914944]},
            ],
            output='screen',
        ),
    ])
