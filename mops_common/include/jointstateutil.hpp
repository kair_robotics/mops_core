#pragma once

#include <vector>
#include <string>

#include <kdl/jntarray.hpp>

#include <sensor_msgs/msg/joint_state.hpp>


/**
 * Utilities to deal with the JointState messages
 */
namespace mops_common { namespace util { // TODO: consider a more distinct namespace name

/**
 * Splits a single JointState message describing the robot configuration
 * into two separate messages based on the prefixes.
 * For example, if the original message contains joints:
 * a_j1, a_j2, b_j1,
 * and the prefixes are a_ and _b respectively,
 * the two returned messages will contain:
 * (a_j1, a_j2) and (b_j1) respectively.
 */
std::pair<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState>
  split_joint_state(
    const sensor_msgs::msg::JointState& m,
    const std::string& prefix1,
    const std::string& prefix2
  )
{
  sensor_msgs::msg::JointState m1;
  sensor_msgs::msg::JointState m2;

  m1.header.stamp = m.header.stamp;
  m2.header.stamp = m.header.stamp;
  
  size_t n_joints = m.name.size();
  if (m.position.size() != n_joints) {
    throw std::runtime_error("Inconsistent joint state message size");
  }

  for (size_t i = 0; i < n_joints; ++i) {
    const std::string& n = m.name[i];
    const double q = m.position[i];
    if (n.find(prefix1) != std::string::npos) {
      m1.name.push_back(n);
      m1.position.push_back(q);
    } else if (n.find(prefix2) != std::string::npos) {
      m2.name.push_back(n);
      m2.position.push_back(q);
    }
  }

  return std::make_pair(m1, m2);
}

/**
 * Merges multiple JointState messages.
 * Adds joint names and joint positions from subsequent messages.
 * Does not check if the joint names are unique.
 * The header is taken from the first message or default when the
 * vector is empty.
 */
sensor_msgs::msg::JointState merge_joint_states(
  const std::vector<sensor_msgs::msg::JointState>& joint_states
) {
  sensor_msgs::msg::JointState merged;
  
  if (!joint_states.empty()) {
    merged.header = joint_states[0].header;
  }
  
  for (const auto& js : joint_states) {
    merged.name.insert(merged.name.end(), js.name.begin(), js.name.end());
    merged.position.insert(merged.position.end(), js.position.begin(), js.position.end());
  }
  
  return merged;
}

/**
 * Converts KDL::JntArray to JointState message.
 */
sensor_msgs::msg::JointState jnt_array_to_joint_state(
  const rclcpp::Time& time,
  const std::vector<std::string>& name,
  const KDL::JntArray& position
) {
  sensor_msgs::msg::JointState joint_state;
  
  joint_state.header.stamp = time;
  joint_state.name = name;
  joint_state.position = std::vector<double>(position.data.data(), position.data.data() + position.data.size());
  
  return joint_state;
}

/**
 * Converts JointState message to KDL::JntArray.
 * Assumes the order according to the position vector.
 */
KDL::JntArray joint_state_to_jnt_array(const sensor_msgs::msg::JointState& joint_state) {
  const size_t n_joints = joint_state.position.size();
  KDL::JntArray jnt_array(n_joints);
  for (size_t i = 0; i < n_joints; ++i) {
    jnt_array(i) = joint_state.position[i];
  }
  return jnt_array;
}


}} // namespace mops_common::util
