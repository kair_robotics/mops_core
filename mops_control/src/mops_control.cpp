/* standard includes */
#include <string>
#include <memory>
#include <optional>

/* library includes */
#include <urdf/model.h>
#include <kdl/tree.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolvervel_wdls.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainjnttojacsolver.hpp>

/* ROS includes */
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>

/* MOPS includes */
#include <mops_common/conversions/kdl.hpp>
#include <mops_common/conversions/eigen.hpp>
#include <mops_common/jointstateutil.hpp>
#include <mops_common/mathutility.hpp>
#include <mops_msgs/msg/tool_end_effector_state_stamped.hpp>
#include <mops_msgs/msg/control_mode.hpp>
#include <mops_msgs/srv/set_control_mode.hpp>


//#define MEASURE_TIME


using namespace std::string_literals;
using namespace std::placeholders;
using namespace std::chrono;
using namespace mops_common::util;



template<>
Eigen::Isometry3d convert_to(const KDL::Frame& m) {
  auto pose = convert_to<geometry_msgs::msg::Pose>(m);
  return convert_to<Eigen::Isometry3d>(pose);
}


double getRotationAngle(const Eigen::Isometry3d& iso) {
    Eigen::Matrix3d rotation_matrix = iso.rotation();
    double trace = rotation_matrix.trace();
    double angle = std::acos((trace - 1) / 2);
    return angle;
}


void set_thread_priority(std::thread &t, int priority) {
    sched_param sch_params;
    sch_params.sched_priority = priority;
    if (pthread_setschedparam(t.native_handle(), SCHED_FIFO, &sch_params)) {
        std::cerr << "Failed to set thread priority" << std::endl;
        std::cerr << "Error code: " << strerror(errno) << std::endl;
    }
}


struct Init
{
    bool ur;
    bool tool;
};


/**
 * Appends the position of the yaw0 joint to the tool JointState.
 * The yaw0 frame is the virtual TCP frame of the tool and it depends
 * on the configurations of the yaw1 and yaw2 joints.
 */
void append_yaw0(sensor_msgs::msg::JointState& m, const std::string& prefix="tool_"s)
{
  // Find indices of yaw1 and yaw2 joints
  std::ptrdiff_t j = -1;
  std::ptrdiff_t k = -1;

  for (std::ptrdiff_t i = m.name.size() - 1; i >= 0; --i) { // expect yaw1 and yaw2 at end
    if ((j == -1) && (m.name[i].rfind("yaw1") != std::string::npos)) {
      j = i;
    } else if ((k == -1) && (m.name[i].rfind("yaw2") != std::string::npos)) {
      k = i;
    }
  }

  if (j != -1 && k != -1) {
    // yaw0 angle is between yaw1 and yaw2
    m.name.push_back(prefix + "yaw0");
    m.position.push_back((m.position[j] - m.position[k]) / 2);
  }
};


/**
 * Calculates distance between two JntArrays
 */
double distance(const KDL::JntArray& q1, const KDL::JntArray& q2) {
  return (q1.data - q2.data).norm();
}


class RcmControlNode : public rclcpp::Node {
public:
  explicit RcmControlNode(const rclcpp::NodeOptions& options = rclcpp::NodeOptions())
    : Node("rcm_control", options)
  {
    /* declare and read parameters */
    std::string robot_description       = declare_parameter("robot_description", ""s);
    std::string ur_prefix               = declare_parameter("ur_prefix", ""s);
    std::string tool_prefix             = declare_parameter("tool_prefix", ""s);
    std::string rcm_prefix              = declare_parameter("rcm_prefix", ""s);
    /* double alpha = */                  declare_parameter("alpha", 0.1);
    /* int max_iterations = */            declare_parameter("max_iterations", 1);
    /* double max_error = */              declare_parameter("max_error", 0.001);
    std::vector<double> tcp_trans_weights   = declare_parameter("tcp_trans_weights", std::vector<double>{1, 1, 1, 1, 1, 1, 0.1, 0.1, 0.1});
    std::vector<double> tcp_rot_weights   = declare_parameter("tcp_rot_weights", std::vector<double>{0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 10, 10, 10});
    std::vector<double> rcm_trans_weights   = declare_parameter("rcm_trans_weights", std::vector<double>{1, 1, 1, 1, 1, 1, 100});
    _control_mode = declare_parameter("control_mode", int(mops_msgs::msg::ControlMode::FREE_MODE));
    
    /* initialize kinematic model */
    if (robot_description.empty()) {
      throw std::runtime_error("The robot_description parameter is empty");
    }
    
    if (!_model.initString(robot_description)) {
      throw std::runtime_error("Failed to initialize urdf::Model from robot_description");
    }
    
    if (!kdl_parser::treeFromUrdfModel(_model, _tree)) {
      throw std::runtime_error("Failed to initialize KDL::Tree from urdf::Model");
    }
    
    // joint limits
    for (const auto& joint : _model.joints_) {
      if (joint.second->limits) {
        _joint_limits[joint.second->name] = *joint.second->limits;
      }
    }
    
    // ur joints
    _ur_base_frame_name = ur_prefix + "base";
    for (const auto& name : {"shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"}) { // TODO: consider making it not hard-coded
      _ur_joint_names.push_back(ur_prefix + name);
    }
    
    _ur_q_current.resize(6); // FIXME: use URDF
    
    // tool joints
    for (const auto& name : {"roll", "pitch", "yaw0", "yaw1", "yaw2"}) { // TODO: consider making it not hard-coded
      _tool_joint_names.push_back(tool_prefix + name);
    }
    
    _tool_q_current.resize(3); // roll pitch yaw0
    
    
    /*
     * configure the RCM chain + solvers
     */
    const std::string rcm_link_name = rcm_prefix + "rcm_link";
    if (!_tree.getChain("world", rcm_link_name, _rcm_chain)) {
      throw std::runtime_error("Failed to find kinematic chain from world to " + rcm_link_name);
    }
    _rcm_njoints = _rcm_chain.getNrOfJoints();
    
    RCLCPP_INFO_STREAM(get_logger(), "RCM chain found:");
    /*for (const auto& seg : _rcm_chain.segments) {
      RCLCPP_INFO_STREAM(get_logger(), " - segment " + seg.getName() + ", joint " + seg.getJoint().getName() + " of type " + seg.getJoint().getTypeName());
    }*/
    
    _rcm_frame_name = rcm_prefix = "rcm";
    _rcm_fk_solver = std::make_unique<KDL::ChainFkSolverPos_recursive>(_rcm_chain);
    _rcm_jac_solver = std::make_unique<KDL::ChainJntToJacSolver>(_rcm_chain);
    _lambda = 0.0;
    _rcm_trans_W = Eigen::VectorXd::Map(rcm_trans_weights.data(), rcm_trans_weights.size()).asDiagonal();
    
    /*
     * configure the TCP chain + solvers
     */
    const std::string tcp_chain_base = "world";
    const std::string tcp_chain_end = tool_prefix + "tcp0";
    if (!_tree.getChain(tcp_chain_base, tcp_chain_end, _tcp_chain)) {
      throw std::runtime_error("Failed to find kinematic chain from " + tcp_chain_base + " to " + tcp_chain_end);
    }
    
    RCLCPP_INFO_STREAM(get_logger(), "TCP chain found:");
    /*for (const auto& seg : _tcp_chain.segments) {
      RCLCPP_INFO_STREAM(get_logger(), " - segment " + seg.getName() + ", joint " + seg.getJoint().getName() + " of type " + seg.getJoint().getTypeName());
    }*/
    _tcp_njoints = _tcp_chain.getNrOfJoints();
    
    _tcp_fk_solver = std::make_unique<KDL::ChainFkSolverPos_recursive>(_tcp_chain);
    _tcp_jac_solver = std::make_unique<KDL::ChainJntToJacSolver>(_tcp_chain);
    
    _tcp_trans_W = Eigen::VectorXd::Map(tcp_trans_weights.data(), tcp_trans_weights.size()).asDiagonal();
    _tcp_rot_W = Eigen::VectorXd::Map(tcp_rot_weights.data(), tcp_rot_weights.size()).asDiagonal();
    
    _init = {false, false};
    
    /* create TF transform listener */
    _tf_buffer = std::make_unique<tf2_ros::Buffer>(get_clock());
    _tf_listener = std::make_unique<tf2_ros::TransformListener>(*_tf_buffer);
    
    /* ROS stuff */
    _low_priority_callback_group = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
    _high_priority_callback_group = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
    
    auto default_subscription_options = rclcpp::SubscriptionOptions();
    default_subscription_options.callback_group = _low_priority_callback_group;
    
    auto servo_subscription_options = rclcpp::SubscriptionOptions();
    servo_subscription_options.callback_group = _high_priority_callback_group;
    
    /* create publishers */
    _pub_robot_move_joint     = create_publisher<sensor_msgs::msg::JointState>("ur/move_joint", 1);
    _pub_tool_move_joint      = create_publisher<sensor_msgs::msg::JointState>("tool/move_joint", 1);
    _pub_robot_servo_joint    = create_publisher<sensor_msgs::msg::JointState>("ur/servo_joint", 1);
    _pub_tool_servo_joint     = create_publisher<sensor_msgs::msg::JointState>("tool/servo_joint", 1);
    _pub_lambda_joint_states  = create_publisher<sensor_msgs::msg::JointState>("lambda/joint_states", 1);
    
    _pub_joint_states = create_publisher<sensor_msgs::msg::JointState>("desired_joint_states", 1);
    
    /* create services */
    _srv_set_control_mode = create_service<mops_msgs::srv::SetControlMode>(
      "~/set_control_mode",
      std::bind(&RcmControlNode::set_control_mode, this, _1, _2),
      rclcpp::ServicesQoS(),
      _low_priority_callback_group
    );
    
    /* create subscribers */
    _subscribers = {
      
      /*
       * Receives servo commands for the desired tool end effector state in the RCM mode
       */
      create_subscription<mops_msgs::msg::ToolEndEffectorStateStamped>(
        "servo_joint_ik",
        rclcpp::QoS(1).best_effort(),
        std::bind(&RcmControlNode::servo_joint_ik_cb, this, _1),
        servo_subscription_options
      ),
      
      /*
       * Receives the current state of the real UR robot
       */
      create_subscription<sensor_msgs::msg::JointState>(
        "ur/joint_states",
        rclcpp::QoS(1).best_effort(),
        std::bind(&RcmControlNode::ur_joint_states_cb, this, _1),
        default_subscription_options
      ),
      
      /*
       * Receives the current state of the tool
       */
      create_subscription<sensor_msgs::msg::JointState>(
        "tool/joint_states",
        rclcpp::QoS(1).best_effort(),
        std::bind(&RcmControlNode::tool_joint_states_cb, this, _1),
        default_subscription_options
      ),
      
    };
    
  }
  
  
  /**
   * Callback for setting the control mode
   */
  void set_control_mode(
    const std::shared_ptr<mops_msgs::srv::SetControlMode::Request> req,
    [[maybe_unused]] std::shared_ptr<mops_msgs::srv::SetControlMode::Response> res
  ) {
    _control_mode = req->mode.mode;
    
    /*if (_control_mode == mops_msgs::msg::ControlMode::FREE_MODE) {
      _lambda = 0.4;
    }*/
  }
  
  
  /** 
   * Callback for the /servo_joint_ik topic to process servo commands 
   * for the desired tool end effector state in the RCM mode
   */
  void servo_joint_ik_cb(mops_msgs::msg::ToolEndEffectorStateStamped::ConstSharedPtr m) {
    
    /* only proceed when both ur and tool are initialized */
    if (!_init.ur) {
      RCLCPP_WARN_STREAM(get_logger(), "Robot not initialized");
      return;
    }
    
    /*if (!_init.tool) {
      RCLCPP_WARN_STREAM(get_logger(), "Tool not initialized");
      return;
    }*/
    
    sensor_msgs::msg::JointState robot_command;
    sensor_msgs::msg::JointState tool_command;
    sensor_msgs::msg::JointState lambda_command;
    
#ifdef MEASURE_TIME
    auto t0 = high_resolution_clock::now();
#endif
    
    if (_control_mode == mops_msgs::msg::ControlMode::FREE_MODE) {
      std::tie(robot_command, tool_command, lambda_command) = get_ik_free_mode_solution(
        convert_to<Eigen::Isometry3d>(m->ee.pose),
        m->ee.grasper_angle
      );
      
    } else if (_control_mode == mops_msgs::msg::ControlMode::RCM_MODE) {
      
      const auto ts_b_rcm = _tf_buffer->lookupTransform("world", _rcm_frame_name, tf2::TimePointZero);
      
      std::tie(robot_command, tool_command, lambda_command) = get_ik_rcm_mode_solution(
        convert_to<Eigen::Isometry3d>(m->ee.pose),
        convert_to<Eigen::Isometry3d>(ts_b_rcm.transform),
        m->ee.grasper_angle
      );
      
      _lambda = lambda_command.position[0];
      
    } else {
      RCLCPP_ERROR(get_logger(), "Unknown control mode!");
      return;
    }
    
#ifdef MEASURE_TIME
    auto t1 = high_resolution_clock::now();
#endif
    
    _pub_robot_servo_joint->publish(robot_command);
    _pub_tool_servo_joint->publish(tool_command);
    _pub_lambda_joint_states->publish(lambda_command);
    
    const auto joint_states = merge_joint_states({robot_command, tool_command, lambda_command});
    _pub_joint_states->publish(joint_states);

#ifdef MEASURE_TIME
    auto duration = duration_cast<microseconds>(t1 - t0);
    std::cout << "Took " << duration.count() << " us" << std::endl;
#endif
  }
  
  
  /**
   * Computes the IK solution for unconstrained movement of TCP.
   * 
   * Returns a tuple containing:
   * - JointState for the robot
   * - JointState for the tool
   * - JointState for the extra joints (e.g. lambda)
   */
  std::tuple<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState>
  get_ik_free_mode_solution(
    const Eigen::Isometry3d& T_b_tcp_desired, const double grasper_angle_desired
  ) {
    
    /* set-up parameters */
    double alpha = get_parameter("alpha").as_double();
    int iterations = 0;
    const int max_iterations = get_parameter("max_iterations").as_int();
    double error = std::numeric_limits<double>::max();
    const double max_error = get_parameter("max_error").as_double();
    
    /* construct current q vector for the TCP task
     * which consists of the UR configuration and the first three tool joints
     */
    KDL::JntArray q_current(9);
    q_current.data << _ur_q_current.data, _tool_q_current.data(Eigen::seq(0, 2));
    KDL::JntArray q_command = q_current;
    
    /* solver loop */
    
    // initializations
    Eigen::VectorXd V_tcp(6);
    Eigen::VectorXd V_b(6);
    KDL::Jacobian J_tcp(_tcp_njoints);
    Eigen::MatrixXd J_tcp_W(6, 9);
    
    while (iterations < max_iterations && error > max_error) {
    
      /* compute error between current tcp pose and the desired tcp pose */
      auto F_b_tcp = get_tcp_fk_solution(q_command);
      Eigen::Isometry3d T_b_tcp = convert_to<Eigen::Isometry3d>(F_b_tcp.value());
      const auto R_b_tcp = T_b_tcp.rotation();
      auto T_error = T_b_tcp.inverse() * T_b_tcp_desired;
      
      V_tcp << T_error(0, 3), T_error(1, 3), T_error(2, 3), T_error(2, 1), T_error(0, 2), T_error(1, 0);
      V_b << R_b_tcp * V_tcp(Eigen::seq(0, 2)), R_b_tcp * V_tcp(Eigen::seq(3, 5));
      
      /* compute jacobian of tcp pose wrt. tcp chain joints */
      
      _tcp_jac_solver->JntToJac(q_current, J_tcp);
      
      // apply joint weights
      const auto J_tcp_M = J_tcp.data;
      J_tcp_W <<
        J_tcp_M(Eigen::seq(0, 2), Eigen::all) * _tcp_trans_W,
        J_tcp_M(Eigen::seq(3, 5), Eigen::all) * _tcp_rot_W;
      
      /* apply joint position modification */
      auto J_tcp_pinv = J_tcp_W.completeOrthogonalDecomposition().pseudoInverse();
      Eigen::VectorXd dq = alpha * J_tcp_pinv * V_b;
      q_command.data += dq;
      
      /* update error */
      ++iterations;
      double tcp_pos_error = sqrt(T_error(0, 3) * T_error(0, 3) + T_error(1, 3) * T_error(1, 3) + T_error(2, 3) * T_error(2, 3));
      double tcp_rot_error = getRotationAngle(T_error);
      
      error = tcp_pos_error + tcp_rot_error;
    }
    
    const auto stamp = now();
    
    KDL::JntArray q_ur_command(6);
    q_ur_command.data << q_command.data(Eigen::seq(0, 5));
    const auto ur_joint_state = jnt_array_to_joint_state(stamp, _ur_joint_names, q_ur_command);
    
    KDL::JntArray q_tool_command(5);
    double q_yaw0 = q_command.data(8);
    double q_yaw1 = q_yaw0 + grasper_angle_desired/2;
    double q_yaw2 = -q_yaw0 + grasper_angle_desired/2;
    q_tool_command.data << q_command.data(Eigen::seq(6, 8)), q_yaw1, q_yaw2;
    const auto tool_joint_state = jnt_array_to_joint_state(stamp, _tool_joint_names, q_tool_command);
    
    KDL::JntArray q_lambda(1);
    q_lambda(0) = _lambda;
    const auto lambda_joint_state = jnt_array_to_joint_state(stamp, {"lambda"}, q_lambda);
    
    return std::make_tuple(ur_joint_state, tool_joint_state, lambda_joint_state);
  }
  
  
  /**
   * Computes the IK solution for RCM constrained movement.
   * 
   * Returns a tuple containing:
   * - JointState for the robot
   * - JointState for the tool
   * - JointState for the extra joints (e.g. lambda)
   */
  std::tuple<sensor_msgs::msg::JointState, sensor_msgs::msg::JointState, sensor_msgs::msg::JointState>
  get_ik_rcm_mode_solution(
    const Eigen::Isometry3d& T_b_tcp_desired,
    const Eigen::Isometry3d& T_b_rcm_desired,
    const double grasper_angle_desired
  ) {
    
    /* set-up parameters */
    double alpha = get_parameter("alpha").as_double();
    int iterations = 0;
    const int max_iterations = get_parameter("max_iterations").as_int();
    double error = std::numeric_limits<double>::max();
    const double max_error = get_parameter("max_error").as_double();
        
    /*
     * construct a complete current q vector consisting of
     * robot q (0-5), tool q (6-8) and lambda (9)
     */
    KDL::JntArray q_all_current(10);
    q_all_current.data << _ur_q_current.data, _tool_q_current.data(Eigen::seq(0, 2)), _lambda;
    KDL::JntArray q_command = q_all_current;
        
    /* solver loop */
    
    // initializations
    KDL::JntArray q_tcp_current(9);
    KDL::JntArray q_rcm_current(7);
    Eigen::VectorXd V_tcp(6);
    Eigen::VectorXd V_b_tcp(6);
    Eigen::VectorXd V_rcm(3);
    Eigen::VectorXd V_b_rcm(3);
    KDL::Jacobian J_tcp(_tcp_njoints);
    KDL::Jacobian J_rcm(_rcm_njoints);
    Eigen::MatrixXd J = Eigen::MatrixXd::Zero(9, 10);
    Eigen::VectorXd V_all(9);
    
    while (iterations < max_iterations && error > max_error) {
      
      /* split the current q vector into q vectors for separate chains */
      q_tcp_current.data << q_command.data(Eigen::seq(0, 8)); // robot q (0-5) + tool q (6-8)
      q_rcm_current.data << q_command.data(Eigen::seq(0, 5)), q_command.data(9); // robot q (0-5) + lambda (6)
      
      /* compute FK and Jacobians */
      auto tcp_fk_future = std::async(std::launch::async, [&]() { return get_tcp_fk_solution(q_tcp_current); });
      auto rcm_fk_future = std::async(std::launch::async, [&]() { return get_rcm_fk_solution(q_rcm_current); });
      auto tcp_jac_future = std::async(std::launch::async, [&]() { return _tcp_jac_solver->JntToJac(q_tcp_current, J_tcp); });
      auto rcm_jac_future = std::async(std::launch::async, [&]() { return _rcm_jac_solver->JntToJac(q_rcm_current, J_rcm); });
          
      /* compute error between current tcp pose and the desired tcp pose */
      auto F_b_tcp = tcp_fk_future.get(); //get_tcp_fk_solution(q_tcp_current);
      Eigen::Isometry3d T_b_tcp = convert_to<Eigen::Isometry3d>(F_b_tcp.value());
      auto R_b_tcp = T_b_tcp.rotation();
      auto T_tcp_error = T_b_tcp.inverse() * T_b_tcp_desired;
      
      V_tcp << T_tcp_error(0, 3), T_tcp_error(1, 3), T_tcp_error(2, 3), T_tcp_error(2, 1), T_tcp_error(0, 2), T_tcp_error(1, 0);
      V_b_tcp.head<3>().noalias() = R_b_tcp * V_tcp.head<3>();
      V_b_tcp.tail<3>().noalias() = R_b_tcp * V_tcp.tail<3>();
       
      /* compute error between current rcm pose and the desired rcm pose */
      auto F_b_rcm = rcm_fk_future.get();
      Eigen::Isometry3d T_b_rcm = convert_to<Eigen::Isometry3d>(F_b_rcm.value());
      auto R_b_rcm = T_b_rcm.rotation();
      auto T_rcm_error = T_b_rcm.inverse() * T_b_rcm_desired;
      
      V_rcm << T_rcm_error(0, 3), T_rcm_error(1, 3), T_rcm_error(2, 3);
      V_b_rcm.noalias() = R_b_rcm * V_rcm;
      
            
      /* construct jacobian */
      tcp_jac_future.wait();
      rcm_jac_future.wait();
            
      // concatenate jacobians
      J.block<6 ,9>(0, 0) = J_tcp.data.block<6, 9>(0, 0);
      J.block<3, 6>(6, 0) = J_rcm.data.block<3, 6>(0, 0);
      J.block<3, 1>(6, 9) = J_rcm.data.block<3, 1>(0, 6);
      
      // apply joint weights
      Eigen::MatrixXd J_W = J;
      J_W.block(0, 0, 3, 9) = J.block(0, 0, 3, 9) * _tcp_trans_W;
      J_W.block(3, 0, 3, 9) = J.block(3, 0, 3, 9) * _tcp_rot_W;
      J_W.block(6, 0, 3, 6) = J.block(6, 0, 3, 6) * _rcm_trans_W.block(0, 0, 6, 6);
      J_W.block(6, 8, 3, 1) = J.block(6, 8, 3, 1) * _rcm_trans_W.block(6, 6, 1, 1);
            
      /* apply joint position modification */
      V_all << V_b_tcp, V_b_rcm;

      auto J_pinv = J_W.completeOrthogonalDecomposition().pseudoInverse();
      
      // BELOW: trying for faster pseudo-inverse - doesn't work, is actually 4-5 times slower
      //auto J_pinv = J_W.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(V_all);
      //Eigen::VectorXd dq = alpha * J_W.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(V_all);
      
      // BELOW: trying to use transpose instead - doesn't work, perhaps need to be transposed block-wise
      //auto J_pinv = J_W.transpose();
      Eigen::VectorXd dq = alpha * J_pinv * V_all;
      
      q_command.data += dq;
            
      /* update error */
      ++iterations;
      {
        double tcp_pos_error = T_tcp_error.translation().norm();
        double tcp_rot_error = getRotationAngle(T_tcp_error);
        double rcm_pos_error = T_rcm_error.translation().norm();
        error = tcp_pos_error + tcp_rot_error + rcm_pos_error;
      }
    }
    
    const auto stamp = now();
    
    KDL::JntArray q_ur_command(6);
    q_ur_command.data << q_command.data(Eigen::seq(0, 5));
    const auto ur_joint_state = jnt_array_to_joint_state(stamp, _ur_joint_names, q_ur_command);
    
    KDL::JntArray q_tool_command(5);
    double q_yaw0 = q_command.data(8);
    double q_yaw1 = q_yaw0 + grasper_angle_desired/2;
    double q_yaw2 = -q_yaw0 + grasper_angle_desired/2;
    q_tool_command.data << q_command.data(Eigen::seq(6, 8)), q_yaw1, q_yaw2;
    const auto tool_joint_state = jnt_array_to_joint_state(stamp, _tool_joint_names, q_tool_command);

    KDL::JntArray q_lambda(1);
    q_lambda(0) = q_command.data(9);
    const auto lambda_joint_state = jnt_array_to_joint_state(stamp, {"lambda"}, q_lambda);
    
    return std::make_tuple(ur_joint_state, tool_joint_state, lambda_joint_state);
  }
  
  
  /**
   * Computes the FK solution for the RCM chain
   */
  std::optional<KDL::Frame> get_rcm_fk_solution(const KDL::JntArray& q) {
    KDL::Frame sol;
    
    auto err = _rcm_fk_solver->JntToCart(q, sol);
    if (err != KDL::ChainFkSolverPos_recursive::E_NOERROR) {
      RCLCPP_WARN_STREAM(get_logger(), "FK error for the RCM chain: " << _rcm_fk_solver->strError(err));
      return {};
    }
    
    return sol;
  }
  
  /**
   * Computes the FK solution for the TCP chain
   */
  std::optional<KDL::Frame> get_tcp_fk_solution(const KDL::JntArray& q) {
    KDL::Frame sol;
    
    auto err = _tcp_fk_solver->JntToCart(q, sol);
    if (err != KDL::ChainFkSolverPos_recursive::E_NOERROR) {
      RCLCPP_WARN_STREAM(get_logger(), "FK error for the TCP chain: " << _tcp_fk_solver->strError(err));
      return {};
    }
    
    return sol;
  }
  
  /** 
   * Callback for the ur/joint_states topic
   */
  void ur_joint_states_cb(sensor_msgs::msg::JointState::ConstSharedPtr m) {
    _ur_q_current = joint_state_to_jnt_array(*m);
    _init.ur = true;
  }
  
  /** 
   * Callback for the tool/joint_states topic
   */
  void tool_joint_states_cb(sensor_msgs::msg::JointState::ConstSharedPtr m) {
    KDL::JntArray q = joint_state_to_jnt_array(*m);
    
    // check if yaw0 in the received state, if not insert
    //if (std::find(m->position.begin(), m->position.end(), _tool_joint_names[2]) == m->position.end()) {
    if (m->position.size() < 5) {
      
      KDL::JntArray q1(5);
      double q_yaw0 = 0.5 * (q.data[2] - q.data[3]);
      
      q1.data << q.data[0], q.data[1], q_yaw0, q.data[2], q.data[3];
      q = q1;
    }
    
    _tool_q_current = q;
    
    _init.tool = true;
  }
  
  rclcpp::CallbackGroup::SharedPtr getLowPriorityCallbackGroup() {
    return _low_priority_callback_group;
  }
  
  rclcpp::CallbackGroup::SharedPtr getHighPriorityCallbackGroup() {
    return _high_priority_callback_group;
  }
  

private:
  unsigned int _control_mode;
  
  std::unique_ptr<tf2_ros::Buffer> _tf_buffer;
  std::unique_ptr<tf2_ros::TransformListener> _tf_listener;

  // kinematic model
  urdf::Model _model;
  KDL::Tree _tree;
  
  KDL::Chain _rcm_chain; // the chain from ur_base to rcm link
  KDL::Chain _tcp_chain; // the chain from ur_base to tool0 link
  
  unsigned int _rcm_njoints;
  unsigned int _tcp_njoints;
  
  std::unique_ptr<KDL::ChainFkSolverPos_recursive> _rcm_fk_solver;
  std::unique_ptr<KDL::ChainFkSolverPos_recursive> _tcp_fk_solver;
  
  std::unique_ptr<KDL::ChainJntToJacSolver> _rcm_jac_solver;
  std::unique_ptr<KDL::ChainJntToJacSolver> _tcp_jac_solver;
  
  std::string _ur_base_frame_name;
  std::vector<std::string> _ur_joint_names;
  std::vector<std::string> _tool_joint_names;
  std::string _rcm_frame_name;
  std::unordered_map<std::string, urdf::JointLimits> _joint_limits;
  
  KDL::JntArray _ur_q_current; // q for ur5 (6x1)
  KDL::JntArray _tool_q_current; // q for the tool (?x1)
  KDL::JntArray _rcm_q_current; // q for ur5 + lambda (7x1)
  KDL::JntArray _tcp_q_current; // q for ur5 + tool (?x1)
  double _lambda;
  
  Eigen::MatrixXd _tcp_trans_W, _tcp_rot_W, _rcm_trans_W;
  
  // ROS
  rclcpp::CallbackGroup::SharedPtr _low_priority_callback_group;
  rclcpp::CallbackGroup::SharedPtr _high_priority_callback_group;
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_robot_move_joint;
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_robot_servo_joint;
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_tool_move_joint;
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_tool_servo_joint;
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_lambda_joint_states;
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_joint_states;
  rclcpp::Service<mops_msgs::srv::SetControlMode>::SharedPtr _srv_set_control_mode;
  std::list<rclcpp::SubscriptionBase::SharedPtr> _subscribers;
  std::list<rclcpp::TimerBase::SharedPtr> _timers;
  
  // other
  Init _init;
};


int main(int argc, char* argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<RcmControlNode>();
  
  // low priority executor
  std::thread low_priority_exec_thread([&node]() {
    rclcpp::executors::SingleThreadedExecutor regular_executor;
    regular_executor.add_callback_group(
        node->getLowPriorityCallbackGroup(), node->get_node_base_interface()
    );
    regular_executor.spin();
  });
  
  // high priority executor
  std::thread high_priority_exec_thread([&node]() {
    rclcpp::executors::SingleThreadedExecutor high_priority_executor;
    high_priority_executor.add_callback_group(
        node->getHighPriorityCallbackGroup(), node->get_node_base_interface()
    );
    high_priority_executor.spin();
  });

  set_thread_priority(high_priority_exec_thread, 99);
  
  low_priority_exec_thread.join();
  high_priority_exec_thread.join();
    
  rclcpp::shutdown();
  return 0;
}
