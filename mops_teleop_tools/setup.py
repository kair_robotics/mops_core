from setuptools import find_packages, setup
import os
from glob import glob

package_name = 'mops_teleop_tools'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob(os.path.join('launch', '*launch.[pxy][yma]*'))),
        (os.path.join('share', package_name, 'rviz'), glob(os.path.join('rviz', '*.rviz'))),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='ubuntu',
    maintainer_email='ubuntu@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
          'mops_teleop_gui = mops_teleop_tools.mops_teleop_gui:main',
          'mops_teleop_joy = mops_teleop_tools.mops_teleop_joy:main',
          'mops_dummy_circle = mops_teleop_tools.mops_dummy_circle:main',
          'mops_teleop_foot = mops_teleop_tools.mops_teleop_foot:main',
          'mops_gui = mops_teleop_tools.mops_gui:main',
        ],
    },
)
