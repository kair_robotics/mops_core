from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.conditions import IfCondition
from launch.substitutions import LaunchConfiguration
from ament_index_python.packages import get_package_share_path


def generate_launch_description():
  rviz_config_path = get_package_share_path('mops_teleop_tools') / 'rviz' / 'mops_teleop_tools.rviz'
  
  rviz_arg = DeclareLaunchArgument('show', default_value='false', description='Whether to launch rviz to visualize input frames')
  
  mops_teleop_gui_node = Node(
    package='mops_teleop_tools',
    executable='mops_teleop_gui',
    parameters=[{
      'rate': 0.008,
      'base_frame': 'world',
      'tcp_frame': 'tool_ee_tcp_desired',
      'pos_offset': [0.100, -0.40, 0.1],
      'rpy_offset': [0.0, 1.57, 0.0],
    }],
    remappings=[
      ('tool_ee_state_desired', 'servo_joint_ik'),
    ],
    output='screen',
  )
  
  rviz = Node(
    package='rviz2',
    executable='rviz2',
    arguments=['-d', f'{rviz_config_path}'],
    condition=IfCondition(LaunchConfiguration('show')),
  )

  return LaunchDescription([
    rviz_arg,
    mops_teleop_gui_node,
    rviz,
  ])
