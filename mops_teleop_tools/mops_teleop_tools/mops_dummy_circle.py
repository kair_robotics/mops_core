import sys
import threading
from math import radians
import math

from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QSlider, QLabel, QPushButton
from PyQt5.QtCore import Qt
from scipy.spatial.transform import Rotation

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Pose, TransformStamped
import tf2_ros

from mops_msgs.msg import ToolEndEffectorStateStamped


class TCPPublisher(Node):
  def __init__(self):
    super().__init__('mops_teleop_gui')
    
    self.rate         = self.declare_parameter('rate', 0.008).value
    self.base_frame   = self.declare_parameter('base_frame', 'world').value
    self.tcp_frame    = self.declare_parameter('tcp_frame', 'tool_ee_tcp_desired').value
    self.pos_offset   = self.declare_parameter('pos_offset', [0.0, 0.0, 0.0]).value
    self.rpy_offset   = self.declare_parameter('rpy_offset', [0.0, 0.0, 0.0]).value
    
    self.ee_state_pub = self.create_publisher(ToolEndEffectorStateStamped, 'tool_ee_state_desired', 10)
    self.br = tf2_ros.TransformBroadcaster(self)
    self.timer = self.create_timer(self.rate, self.timer_cb)
    
    self.pos                 = [0.0, 0.0, 0.0]
    self.rpy                 = [0.0, 0.0, 0.0]
    self.grasper_angle       = 0.0
    
    self.t = 0.0
    
  def timer_cb(self):
    self.pos_offset = self.get_parameter('pos_offset').value
    self.rpy_offset = self.get_parameter('rpy_offset').value
    
    pos = [i+j for i, j in zip(self.pos, self.pos_offset)]
    rpy = [i+j for i, j in zip(self.rpy, self.rpy_offset)]
    #print(rpy)
    R = Rotation.from_euler('XYZ', rpy)
    Q = R.as_quat()
    
    T = TransformStamped()
    T.header.stamp = self.get_clock().now().to_msg()
    T.header.frame_id = self.base_frame
    T.child_frame_id = self.tcp_frame
    T.transform.translation.x = pos[0]
    T.transform.translation.y = pos[1]
    T.transform.translation.z = pos[2]
    T.transform.rotation.x = Q[0]
    T.transform.rotation.y = Q[1]
    T.transform.rotation.z = Q[2]
    T.transform.rotation.w = Q[3]
    self.br.sendTransform(T)
    
    S = ToolEndEffectorStateStamped()
    S.header = T.header
    S.ee.pose.position.x = pos[0]
    S.ee.pose.position.y = pos[1]
    S.ee.pose.position.z = pos[2]
    S.ee.pose.orientation.x = Q[0]
    S.ee.pose.orientation.y = Q[1]
    S.ee.pose.orientation.z = Q[2]
    S.ee.pose.orientation.w = Q[3]
    S.ee.grasper_angle = self.grasper_angle
    self.ee_state_pub.publish(S)
    
    x = 0.025 * math.sin(0.4 * self.t)
    y = 0.025 * math.cos(0.4 * self.t)
    
    self.pos = [x, y, -0.1]
    
    self.t += 0.008
        


def main(args=None):
  rclpy.init(args=args)
  rclpy.spin(TCPPublisher())
  rclpy.shutdown()
