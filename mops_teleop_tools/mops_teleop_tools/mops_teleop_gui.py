import sys
import threading
from math import radians

from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QSlider, QLabel, QPushButton
from PyQt5.QtCore import Qt
from scipy.spatial.transform import Rotation

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Pose, TransformStamped
import tf2_ros

from mops_msgs.msg import ToolEndEffectorStateStamped


class TCPPublisher(Node):
  def __init__(self):
    super().__init__('mops_teleop_gui')
    
    self.rate         = self.declare_parameter('rate', 0.008).value
    self.base_frame   = self.declare_parameter('base_frame', 'world').value
    self.tcp_frame    = self.declare_parameter('tcp_frame', 'tool_ee_tcp_desired').value
    self.pos_offset   = self.declare_parameter('pos_offset', [0.0, 0.0, 0.0]).value
    self.rpy_offset   = self.declare_parameter('rpy_offset', [0.0, 0.0, 0.0]).value
    
    self.ee_state_pub = self.create_publisher(ToolEndEffectorStateStamped, 'tool_ee_state_desired', 10)
    self.br = tf2_ros.TransformBroadcaster(self)
    self.timer = self.create_timer(self.rate, self.timer_cb)
    
    self.pos                 = [0.0, 0.0, 0.0]
    self.rpy                 = [0.0, 0.0, 0.0]
    self.grasper_angle       = 0.0
    
  def timer_cb(self):
    self.pos_offset = self.get_parameter('pos_offset').value
    self.rpy_offset = self.get_parameter('rpy_offset').value
    
    pos = [i+j for i, j in zip(self.pos, self.pos_offset)]
    rpy = [i+j for i, j in zip(self.rpy, self.rpy_offset)]
    #print(rpy)
    R = Rotation.from_euler('XYZ', rpy)
    Q = R.as_quat()
    
    T = TransformStamped()
    T.header.stamp = self.get_clock().now().to_msg()
    T.header.frame_id = self.base_frame
    T.child_frame_id = self.tcp_frame
    T.transform.translation.x = pos[0]
    T.transform.translation.y = pos[1]
    T.transform.translation.z = pos[2]
    T.transform.rotation.x = Q[0]
    T.transform.rotation.y = Q[1]
    T.transform.rotation.z = Q[2]
    T.transform.rotation.w = Q[3]
    self.br.sendTransform(T)
    
    S = ToolEndEffectorStateStamped()
    S.header = T.header
    S.ee.pose.position.x = pos[0]
    S.ee.pose.position.y = pos[1]
    S.ee.pose.position.z = pos[2]
    S.ee.pose.orientation.x = Q[0]
    S.ee.pose.orientation.y = Q[1]
    S.ee.pose.orientation.z = Q[2]
    S.ee.pose.orientation.w = Q[3]
    S.ee.grasper_angle = self.grasper_angle
    self.ee_state_pub.publish(S)
        

class TCPGUI(QMainWindow):
  def __init__(self):
    super().__init__()
    self.node = TCPPublisher()
    self.ros_thread = threading.Thread(
      target=self.ros_loop,
      daemon=True
    )
    self.ros_thread.start()
    
    self.setWindowTitle('TCP Publisher GUI')
    self.setGeometry(100, 100, 400, 300)  # Set window position and size

    self.central_widget = QWidget()
    self.setCentralWidget(self.central_widget)
    self.layout = QVBoxLayout(self.central_widget)

    # Create sliders for x, y, z
    self.sliders = []
    for i in range(3):  
      slider = QSlider(Qt.Horizontal)
      slider.setRange(-1000, 1000)
      slider.setValue(0)
      slider.valueChanged.connect(self.position_slider_changed)
      self.layout.addWidget(QLabel(f'{["x", "y", "z"][i]}:'))
      self.layout.addWidget(slider)
      self.sliders.append(slider)

    # Create sliders for roll, pitch, yaw
    self.rotation_sliders = []
    for i in range(3):  
      slider = QSlider(Qt.Horizontal)
      slider.setRange(-180, 180)
      slider.setValue(0)
      slider.valueChanged.connect(self.rotation_slider_changed)
      self.layout.addWidget(QLabel(f'{["roll", "pitch", "yaw"][i]}:'))
      self.layout.addWidget(slider)
      self.rotation_sliders.append(slider)
    
    self.grasper_angle_slider = QSlider(Qt.Horizontal)
    self.grasper_angle_slider.setRange(0, 180)
    self.grasper_angle_slider.setValue(0)
    self.grasper_angle_slider.valueChanged.connect(self.grasper_angle_slider_changed)
    self.layout.addWidget(QLabel(f'grasper angle'))
    self.layout.addWidget(self.grasper_angle_slider)

    self.center_button = QPushButton('Center sliders', self)
    self.layout.addWidget(self.center_button)
    self.center_button.clicked.connect(self.center_sliders)
  
  def ros_loop(self):
    while rclpy.ok():
      try:
        rclpy.spin_once(self.node)
      except:
        self.close()

  def position_slider_changed(self):
    self.node.pos = [slider.value() / 10000.0 for slider in self.sliders]

  def rotation_slider_changed(self):
    self.node.rpy = [radians(slider.value()) for slider in self.rotation_sliders]
  
  def grasper_angle_slider_changed(self):
    self.node.grasper_angle = radians(self.grasper_angle_slider.value())

  def center_sliders(self):
    for slider in self.sliders:
      slider.setValue(0)
    for slider in self.rotation_sliders:
      slider.setValue(0)
    self.grasper_angle_slider.setValue(0)


def main(args=None):
  rclpy.init(args=args)
  
  app = QApplication(sys.argv)
  gui = TCPGUI()
  gui.show()
  
  sys.exit(app.exec())
  rclpy.shutdown()
