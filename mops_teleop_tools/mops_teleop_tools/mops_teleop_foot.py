import rclpy
from rclpy.node import Node

from touch_msgs.msg import ButtonEvent
from std_msgs.msg import Bool


class FootPublisher(Node):
  def __init__(self):
    super().__init__('mops_teleop_foot')
    
    self.middle_sub = self.create_subscription(Bool, 'middle', self.middle_cb, 1)
    self.right_sub = self.create_subscription(Bool, 'right', self.right_cb, 1)
    self.buttons_pub = self.create_publisher(ButtonEvent, 'buttons_received', 10)

  def middle_cb(self, msg):
    mb = ButtonEvent()
    mb.button = 1
    mb.event = 1 if msg.data else -1
    self.buttons_pub.publish(mb)
  
  def right_cb(self, msg):
    mb = ButtonEvent()
    mb.button = 2
    mb.event = 1 if msg.data else -1
    self.buttons_pub.publish(mb)
    
      

def main(args=None):
  rclpy.init(args=args)
  rclpy.spin(FootPublisher())
  rclpy.shutdown()
