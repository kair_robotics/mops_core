import sys
import threading
from math import radians

from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QSlider, QLabel, QPushButton
from PyQt5.QtCore import Qt
from scipy.spatial.transform import Rotation

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Pose, TransformStamped
from sensor_msgs.msg import Joy
import tf2_ros

from mops_msgs.msg import ToolEndEffectorStateStamped


class TeleopJoy(Node):
  def __init__(self):
    super().__init__('mops_teleop_joy')
    
    self.rate             = self.declare_parameter('rate', 0.008).value
    self.base_frame       = self.declare_parameter('base_frame', 'world').value
    self.tcp_frame        = self.declare_parameter('tcp_frame', 'tool_ee_tcp_desired').value
    self.pos_offset       = self.declare_parameter('pos_offset', [0.0, 0.0, 0.0]).value
    self.rpy_offset       = self.declare_parameter('rpy_offset', [0.0, 0.0, 0.0]).value
    self.x_axis_index     = self.declare_parameter('x_axis_index', 0).value
    self.x_axis_mult      = self.declare_parameter('x_axis_mult', -0.01).value
    self.y_axis_index     = self.declare_parameter('y_axis_index', 1).value
    self.y_axis_mult      = self.declare_parameter('y_axis_mult', 0.01).value
    self.z_axis_index     = self.declare_parameter('z_axis_index', 5).value
    self.z_axis_mult      = self.declare_parameter('z_axis_mult', 0.01).value
    self.roll_axis_index  = self.declare_parameter('roll_axis_index', 3).value
    self.roll_axis_mult   = self.declare_parameter('roll_axis_mult', -0.5).value
    self.pitch_axis_index = self.declare_parameter('pitch_axis_index', 2).value
    self.pitch_axis_mult  = self.declare_parameter('pitch_axis_mult', 0.5).value
    self.yaw_axis_index   = self.declare_parameter('yaw_axis_index', 4).value
    self.yaw_axis_mult    = self.declare_parameter('yaw_axis_mult', 0.5).value
    self.grasper_open_index= self.declare_parameter('grasper_open_index', 2).value
    self.grasper_open_mult= self.declare_parameter('grasper_open_mult', 0.5).value
    self.grasper_close_index= self.declare_parameter('grasper_close_index', 3).value
    self.grasper_close_mult= self.declare_parameter('grasper_close_mult', -0.5).value
    
    self.ee_state_pub = self.create_publisher(ToolEndEffectorStateStamped, 'tool_ee_state_desired', 10)
    self.br = tf2_ros.TransformBroadcaster(self)
    self.timer = self.create_timer(self.rate, self.timer_cb)
    
    self.joy_sub = self.create_subscription(Joy, 'joy', self.joy_cb, 10)
    
    self.pos                 = [0.0, 0.0, 0.0]
    self.rpy                 = [0.0, 0.0, 0.0]
    self.grasper_angle       = 0.0
    self.v                   = [0.0, 0.0, 0.0]
    self.w                   = [0.0, 0.0, 0.0]
    self.dgrasper_angle      = 0.0
    
  def timer_cb(self):
    self.pos_offset = self.get_parameter('pos_offset').value
    self.rpy_offset = self.get_parameter('rpy_offset').value
    
    self.pos[0] += self.rate * self.v[0]
    self.pos[1] += self.rate * self.v[1]
    self.pos[2] += self.rate * self.v[2]
    
    self.rpy[0] += self.rate * self.w[0]
    self.rpy[1] += self.rate * self.w[1]
    self.rpy[2] += self.rate * self.w[2]
    
    self.grasper_angle += self.rate * self.dgrasper_angle
    if self.grasper_angle < 0.0:
      self.grasper_angle = 0.0
    elif self.grasper_angle > 3.14:
      self.grasper_angle = 3.14
    
    pos = [i+j for i, j in zip(self.pos, self.pos_offset)]
    rpy = [i+j for i, j in zip(self.rpy, self.rpy_offset)]

    R = Rotation.from_euler('XYZ', rpy)
    Q = R.as_quat()
    
    T = TransformStamped()
    T.header.stamp = self.get_clock().now().to_msg()
    T.header.frame_id = self.base_frame
    T.child_frame_id = self.tcp_frame
    T.transform.translation.x = pos[0]
    T.transform.translation.y = pos[1]
    T.transform.translation.z = pos[2]
    T.transform.rotation.x = Q[0]
    T.transform.rotation.y = Q[1]
    T.transform.rotation.z = Q[2]
    T.transform.rotation.w = Q[3]
    self.br.sendTransform(T)
    
    S = ToolEndEffectorStateStamped()
    S.header = T.header
    S.ee.pose.position.x = pos[0]
    S.ee.pose.position.y = pos[1]
    S.ee.pose.position.z = pos[2]
    S.ee.pose.orientation.x = Q[0]
    S.ee.pose.orientation.y = Q[1]
    S.ee.pose.orientation.z = Q[2]
    S.ee.pose.orientation.w = Q[3]
    S.ee.grasper_angle = self.grasper_angle
    self.ee_state_pub.publish(S)
  
  def joy_cb(self, msg):
    self.v[0] = msg.axes[self.x_axis_index] * self.x_axis_mult
    self.v[1] = msg.axes[self.y_axis_index] * self.y_axis_mult
    self.v[2] = msg.axes[self.z_axis_index] * self.z_axis_mult
    
    self.w[0] = msg.axes[self.roll_axis_index] * self.roll_axis_mult
    self.w[1] = msg.axes[self.pitch_axis_index] * self.pitch_axis_mult
    self.w[2] = msg.axes[self.yaw_axis_index] * self.yaw_axis_mult
    
    self.dgrasper_angle = msg.buttons[self.grasper_open_index] * self.grasper_open_mult + \
      msg.buttons[self.grasper_close_index] * self.grasper_close_mult


def main(args=None):
  rclpy.init(args=args)
  
  node = TeleopJoy()
  rclpy.spin(node)

  node.destroy_node()
  rclpy.shutdown()
