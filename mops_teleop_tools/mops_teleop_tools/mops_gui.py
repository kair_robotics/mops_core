import sys
import threading

from PyQt5.QtWidgets import QApplication, QMainWindow, QGridLayout, QWidget, QSlider, QLabel, QPushButton, QSpacerItem, QSizePolicy
from PyQt5.QtCore import Qt

import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool, Float32
from mops_msgs.srv import SetControlMode

#from tf2_ros import TransformStamped
from geometry_msgs.msg import TransformStamped
from tf2_ros import TransformException
from tf2_ros import TransformBroadcaster
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener

import numpy as np
from scipy.spatial.transform import Rotation



# FIXME: there must be a better way to do this
def transformstamped2matrix(msg):
  p = np.array([[
    msg.transform.translation.x,
    msg.transform.translation.y,
    msg.transform.translation.z
  ]]).T
  
  q = np.array([
    msg.transform.rotation.x,
    msg.transform.rotation.y,
    msg.transform.rotation.z,
    msg.transform.rotation.w
  ])
  
  rot = Rotation.from_quat(q)
  R = rot.as_matrix()
  
  T = np.vstack((np.hstack((R, p)), np.array([0, 0, 0, 1])))
  return T

def matrix2transformstamped(T, parent, child):
  msg = TransformStamped()
  msg.header.frame_id = parent
  msg.child_frame_id = child
  msg.transform.translation.x = T[0, 3]
  msg.transform.translation.y = T[1, 3]
  msg.transform.translation.z = T[2, 3]
  rot = Rotation.from_matrix(T[0:3, 0:3])
  q = rot.as_quat()
  msg.transform.rotation.x = q[0]
  msg.transform.rotation.y = q[1]
  msg.transform.rotation.z = q[2]
  msg.transform.rotation.w = q[3]
  return msg

def Trans(x, y, z):
  T = np.zeros((4, 4))
  T[0, 3] = x
  T[1, 3] = y
  T[2, 3] = z
  T[3, 3] = 1.0
  return T


class MopsGUINode(Node):
  def __init__(self):
    super().__init__('mops_gui')
    
    self.tf_buffer = Buffer()
    self.tf_listener = TransformListener(self.tf_buffer, self)
    self.tf_broadcaster = TransformBroadcaster(self)
    
    self.set_control_mode_client = self.create_client(SetControlMode, '/rcm_control/set_control_mode')
    self.clutch_pub = self.create_publisher(Bool, 'clutch_engaged', 10)
    self.speed_pub = self.create_publisher(Float32, 'translation_scaling', 10)
    
  def set_control_mode(self, mode):
    req = SetControlMode.Request()
    req.mode.mode = mode
    self.set_control_mode_client.call_async(req)
    
  def timer_cb(self):
    pass
        

class MopsMainWindow(QMainWindow):
  def __init__(self):
    super().__init__()
    
    self.node = MopsGUINode()
    self.ros_thread = threading.Thread(
      target=self.ros_loop,
      daemon=True
    )
    self.ros_thread.start()
    
    self.setWindowTitle('MOPS')
    self.setGeometry(100, 100, 400, 300)  # Set window position and size

    self.central_widget = QWidget()
    self.setCentralWidget(self.central_widget)
    self.layout = QGridLayout(self.central_widget)
    
    self.clutch = False
    self.clutch_button = QPushButton('Clutch: OFF', self)
    self.clutch_button.clicked.connect(self.clutch_button_clicked)
    self.layout.addWidget(self.clutch_button)

    self.control_mode = 0
    self.control_mode_button = QPushButton('Control mode: FREE', self)
    self.control_mode_button.clicked.connect(self.control_mode_button_clicked)
    self.layout.addWidget(self.control_mode_button)
    
    self.speed = 1.0
    self.speed_slider = QSlider(Qt.Orientation.Horizontal, self)
    self.speed_slider.setRange(0, 100)
    self.speed_slider.setValue(100)
    self.speed_slider.setSingleStep(1)
    self.speed_slider.setPageStep(10)
    self.speed_slider.valueChanged.connect(self.speed_slider_value_changed)
    self.layout.addWidget(self.speed_slider)
    
    self.layout.addItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))
    
    self.T_world_rcm = None
    
  def ros_loop(self):
    while rclpy.ok():
      try:
        rclpy.spin_once(self.node)
      except:
        self.close()
        
  def clutch_button_clicked(self):
    if not self.clutch:
      self.clutch = True
      self.clutch_button.setText('Clutch: ON')
    else:
      self.clutch = False
      self.clutch_button.setText('Clutch: OFF')
    
    self.node.clutch_pub.publish(Bool(data=self.clutch))
  
  def control_mode_button_clicked(self):
    if self.control_mode == 0:
      
      # when setting RCM mode, find the RCM point pose on the tool and set as the RCM task location
      try:
        TS_w_tool_base = self.node.tf_buffer.lookup_transform(
          'world',
          'tool_base',
          rclpy.time.Time()
        )
        T_w_tool_base = transformstamped2matrix(TS_w_tool_base)
        T_w_rcm = T_w_tool_base @ Trans(0.35, 0, 0)
        TS_w_rcm = matrix2transformstamped(T_w_rcm, 'world', 'rcm')
        self.node.tf_broadcaster.sendTransform(TS_w_rcm)
        
      except TransformException as ex:
        self.get_logger().error(f"Could not find transform from 'world' to 'rcm_link': {ex}")
        return
      
      #self.T_world_rcm.child_frame_id = 'rcm'  
      #self.node.tf_broadcaster.sendTransform(self.T_world_rcm)
        
      self.control_mode = 1
      self.control_mode_button.setText('Control mode: RCM')
      
      # set the TCP initial pose wrt. RCM task as well
      
    elif self.control_mode == 1:
      self.control_mode = 0
      self.control_mode_button.setText('Control mode: FREE')
      
    self.node.set_control_mode(self.control_mode)
    
  def speed_slider_value_changed(self, value):
    speed = 0.01 * value
    self.node.speed_pub.publish(Float32(data=speed))


def main(args=None):
  rclpy.init(args=args)
  
  app = QApplication(sys.argv)
  main_window = MopsMainWindow()
  main_window.show()
  
  sys.exit(app.exec())
  rclpy.shutdown()
