import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool
import evdev
import select


class MopsFootpedalNode(Node):
  def __init__(self):
    super().__init__('mops_footpedal')
    
    self.declare_parameter('device', '/dev/input/by-id/usb-PCsensor_FootSwitch-event-kbd')
    
    self.device_path = self.get_parameter('device').value
    self.device = None
    try:
      self.device = evdev.InputDevice(self.device_path)
      self.device.grab()
      self.get_logger().info(f"Connected to {self.device.name} at {self.device_path}")
    except FileNotFoundError:
      self.get_logger().error(f"Footpedal not found at {self.device_path}")
      raise
    
    self.clutch_pub = self.create_publisher(Bool, 'clutch', 10)
    self.not_clutch_pub = self.create_publisher(Bool, 'not_clutch', 10)
    self.timer = self.create_timer(0.01, self.timer_cb)
    
  def __del__(self):
    if self.device is not None:
      self.device.ungrab()
    
  def timer_cb(self):
    r, _, _ = select.select([self.device], [], [], 0)
    if r:
      for event in self.device.read():
        if event.type == evdev.ecodes.EV_KEY:
          if event.value == evdev.events.KeyEvent.key_down:
            self.clutch_pub.publish(Bool(data=True))
            self.not_clutch_pub.publish(Bool(data=False))
          elif event.value == evdev.events.KeyEvent.key_up:
            self.clutch_pub.publish(Bool(data=False))
            self.not_clutch_pub.publish(Bool(data=True))
          else:
            pass
            

def main(args=None):
  rclpy.init(args=args)
  node = MopsFootpedalNode()
  rclpy.spin(node)
  rclpy.shutdown()


if __name__ == "__main__":
  main()
