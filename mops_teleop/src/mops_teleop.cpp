﻿#include "mops_common/conversions/eigen.hpp"
#include "mops_common/mathutility.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "std_msgs/msg/bool.hpp"
#include "std_msgs/msg/float32.hpp"
#include "touch_msgs/msg/button_event.hpp"
#include "mops_msgs/msg/tool_end_effector_state_stamped.hpp"
#include <mops_msgs/msg/orientation_mode.hpp>
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"
#include "rclcpp/rclcpp.hpp"

#include <Eigen/Geometry>

#include <chrono>
#include <thread>

using namespace std::chrono_literals;
using namespace std::string_literals;

namespace mops_teleop {

template<typename T>
auto sec_to_dur(T seconds) {
    return std::chrono::duration<T, std::ratio<1>>(seconds);
}

class TeleopNode : public rclcpp::Node
{
public:
    explicit TeleopNode(const rclcpp::NodeOptions& options = rclcpp::NodeOptions())
        : rclcpp::Node("teleop", options)
        , init_current_(true)
        , clutch_engaged_(false)
        //, _orientation_mode(mops_msgs::msg::OrientationMode::INCREMENTAL_MODE)
        , t_robotbase_robottcp_current_(Eigen::Isometry3d::Identity())
        , t_robotbase_robottcp_desired_(t_robotbase_robottcp_current_)
        , t_robotbase_haptictcp_current_(Eigen::Isometry3d::Identity())
        , t_robotbase_haptictcp_last_(t_robotbase_haptictcp_current_)
        , t_robotbase_hapticbase_(Eigen::Isometry3d::Identity())
        , buttons_({false, false})
        , grasp_current_(math::radians(60))
        , grasp_desired_(grasp_current_)
        , grasp_rate_(math::pi / 4)
        , _translation_scaling(1.0)
    {
        auto robot_base = declare_parameter("ur_prefix", ""s) + "base_link";
        auto haptic_base = declare_parameter("haptic_prefix", ""s) + "base";
        _orientation_mode = declare_parameter("orientation_mode", 0);

        t_robotbase_hapticbase_ = [&]() {
            tf2_ros::Buffer tf_buffer(get_clock());
            tf2_ros::TransformListener tf_listener(tf_buffer);
            auto deadline = std::chrono::steady_clock::now() + 5s;

            while (std::chrono::steady_clock::now() < deadline) {
                try {
                    auto tf = tf_buffer.lookupTransform(robot_base, haptic_base, tf2::TimePointZero);
                    return convert_to<Eigen::Isometry3d>(tf.transform);
                } catch (const tf2::LookupException&) {
                    std::this_thread::sleep_for(200ms);
                }
            }

            throw std::runtime_error("Failed to get transform from '" + robot_base + "' to '" + haptic_base + "'");
        }();

        pub_ee_desired_ = create_publisher<mops_msgs::msg::ToolEndEffectorStateStamped>("ee_state_desired", 1);

        subscribers_ = {
            create_subscription<std_msgs::msg::Float32>(
              "translation_scaling",
              10,
              [this](std_msgs::msg::Float32::ConstSharedPtr m) {
                _translation_scaling = m->data;
              }
            ),
            
            create_subscription<std_msgs::msg::Bool>(
                "clutch_engaged",
                10,
                [this](std_msgs::msg::Bool::ConstSharedPtr m) {
                    // Initially: desired <- current
                    if (m->data) {
                        t_robotbase_robottcp_desired_ = t_robotbase_robottcp_current_;
                        // FIXME: this is not nice because grasper angle computed
                        // from kinematics is quite inaccurate
                        grasp_desired_ = grasp_current_;
                        clutch_engaged_ = true;
                    } else {
                        clutch_engaged_ = false;
                    }
                }
            ),
            create_subscription<mops_msgs::msg::ToolEndEffectorStateStamped>(
                "ee_state_current",
                rclcpp::QoS(1).best_effort(),
                [this](mops_msgs::msg::ToolEndEffectorStateStamped::ConstSharedPtr m) {
                    // Cache the most recent end-effector state computed from forward kinematics
                    t_robotbase_robottcp_current_ = convert_to<Eigen::Isometry3d>(m->ee.pose);
                    grasp_current_ = m->ee.grasper_angle;
                    init_current_ = true;
                }
            ),
            create_subscription<geometry_msgs::msg::PoseStamped>(
                "haptic_pose",
                rclcpp::QoS(1).best_effort(),
                [this](geometry_msgs::msg::PoseStamped::ConstSharedPtr m) {
                    haptic_pose_update(m->pose);
                }
            ),
            create_subscription<mops_msgs::msg::ToolEndEffectorStateStamped>(
              "ee_state_haptic",
              rclcpp::QoS(1).best_effort(),
              [this](mops_msgs::msg::ToolEndEffectorStateStamped::ConstSharedPtr m) {
                haptic_pose_update(m->ee.pose);
                grasp_desired_ = m->ee.grasper_angle;
              }
            ),
            create_subscription<touch_msgs::msg::ButtonEvent>(
                "haptic_buttons",
                10,
                [this](touch_msgs::msg::ButtonEvent::ConstSharedPtr m) {
                    if (m->button == touch_msgs::msg::ButtonEvent::BUTTON_GRAY)
                        buttons_[0] = (m->event == touch_msgs::msg::ButtonEvent::EVENT_PRESSED);
                    else if (m->button == touch_msgs::msg::ButtonEvent::BUTTON_WHITE)
                        buttons_[1] = (m->event == touch_msgs::msg::ButtonEvent::EVENT_PRESSED);
                }
            ),
            
            create_subscription<mops_msgs::msg::OrientationMode>(
              "orientation_mode",
              10,
              [this](mops_msgs::msg::OrientationMode::ConstSharedPtr m) {
                this->_orientation_mode = m->mode;
              }
            ),
        };

        stamp_prev_ = now();

        timer_ = create_wall_timer(
            sec_to_dur(1.0 / declare_parameter("publish_rate", 125.0)),
            [this]() {
                if (!init_current_ || !clutch_engaged_)
                    return;

                auto stamp_now = now();
                auto dt = (stamp_now - stamp_prev_).seconds();
                stamp_prev_ = stamp_now;

                if (buttons_[0] && !buttons_[1]) {
                    grasp_desired_ -= grasp_rate_ * dt;
                } else if (buttons_[1] && !buttons_[0]) {
                    grasp_desired_ += grasp_rate_ * dt;
                }

                auto m = std::make_unique<mops_msgs::msg::ToolEndEffectorStateStamped>();
                
                m->ee.pose = convert_to<geometry_msgs::msg::Pose>(t_robotbase_robottcp_desired_);
                m->ee.grasper_angle = grasp_desired_;
                pub_ee_desired_->publish(std::move(m));
            }
        );
    }
    
    void haptic_pose_update(const geometry_msgs::msg::Pose& pose) {
      t_robotbase_haptictcp_last_ = std::move(t_robotbase_haptictcp_current_);

      // Transform incoming haptic TCP poses seen wrt. haptic base
      // frame to the robot base coordinate system
      t_robotbase_haptictcp_current_ = t_robotbase_hapticbase_ * convert_to<Eigen::Isometry3d>(pose);

      if (clutch_engaged_) {
        
        if (_orientation_mode == mops_msgs::msg::OrientationMode::INCREMENTAL_MODE) {
          // Transform from the previous to the current haptic pose
          Eigen::Isometry3d t_incr = t_robotbase_haptictcp_last_.inverse() * t_robotbase_haptictcp_current_;
          t_incr.translation() *= _translation_scaling;

          // "added to" the desired robot TCP pose
          t_robotbase_robottcp_desired_ = t_robotbase_robottcp_desired_ * t_incr;
          
        } else if (_orientation_mode == mops_msgs::msg::OrientationMode::ABSOLUTE_MODE) {
          // haptic tcp change in haptic tcp frame of reference
          Eigen::Isometry3d t_incr = t_robotbase_haptictcp_last_.inverse() * t_robotbase_haptictcp_current_;
          t_incr.translation() *= _translation_scaling;
          
          t_robotbase_robottcp_desired_ = t_robotbase_robottcp_desired_ * t_incr;
          t_robotbase_robottcp_desired_.linear() = t_robotbase_haptictcp_current_.linear();
          
        } else {
          RCLCPP_ERROR_STREAM(get_logger(), "Unsupported orientation mode: " << _orientation_mode << std::endl);
        }
      }
    }

private:
    bool init_current_;
    bool clutch_engaged_;
    unsigned int _orientation_mode;
    Eigen::Isometry3d t_robotbase_robottcp_current_;
    Eigen::Isometry3d t_robotbase_robottcp_desired_;
    Eigen::Isometry3d t_robotbase_haptictcp_current_;
    Eigen::Isometry3d t_robotbase_haptictcp_last_;
    Eigen::Isometry3d t_robotbase_hapticbase_;
    std::array<bool, 2> buttons_;
    double grasp_current_;
    double grasp_desired_;
    double grasp_rate_;
    rclcpp::Publisher<mops_msgs::msg::ToolEndEffectorStateStamped>::SharedPtr pub_ee_desired_;
    std::list<rclcpp::SubscriptionBase::SharedPtr> subscribers_;
    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Time stamp_prev_;
    double _translation_scaling;
};

} // namespace mops_teleop

int main(int argc, char* argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<mops_teleop::TeleopNode>();
  rclcpp::spin(node);
  rclcpp::shutdown();
  return 0;
}
